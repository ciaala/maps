module.exports = function (grunt) {

    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                "esversion": 6
            }
        },
        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'concat']
        },
        concat: {
            options: {
                separator: '\n// ---\n'
            },
            dist: {
                src: ['src/**/*.js'],
                dest: 'dist/maps.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.log.writeln(new Date());
    grunt.registerTask('default', ['jshint', 'concat']);

};