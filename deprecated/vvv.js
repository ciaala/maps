(function() {
    var googleMap = undefined;
    "use strict";
    var getHTML = function(url, index) {
        var request = new XMLHttpRequest();
        request.responseType = 'document';
        //request.overrideMimeType('text/xml');

        return new Promise(function(resolve, fail) {
                request.onload = function() {
                    resolve({
                        document: request.responseXML,
                        url: url,
                        index: index
                    });
                }
                request.onerror = function() {
                    fail("error");
                }
                request.open("GET", url, true);
                request.send();
            }
        );

    };
    (function initGoogleMap() {
        var scriptBlock = document.getElementById('scriptBlock');
        if (scriptBlock === null || scriptBlock === undefined) {
            var srcString = 'https://maps.googleapis.com/maps/api/js?key={YOUR_API_KEY}&callback=initMap';
            //var key = 'AIzaSyBrtWALRhFKan4_v9ELNR_8MIj0Jbv6Bzg';
            var key = 'AIzaSyCvrW2I6KxWZSCWOWAxgSzOZ0nIXmJZDao';
            srcString = srcString.replace('{YOUR_API_KEY}', key);

            scriptBlock = document.createElement('script');
            scriptBlock.setAttribute('id', 'scriptBlock');
            scriptBlock.setAttribute('src', srcString);
            scriptBlock.setAttribute('defer', true);
            scriptBlock.setAttribute('async', true);

            document.body.append(scriptBlock);
        }
        var mapDiv = document.getElementById('myMap');
        if (mapDiv === undefined || mapDiv === null) {
            mapDiv = document.createElement('div');

            var titleElement = document.getElementById('listBreadcrumb');
            titleElement.parentNode.insertBefore(mapDiv, titleElement);
        }
        mapDiv.setAttribute('id', 'myMap')
        mapDiv.style.height = '800px';
        mapDiv.style.width = '1280px';
        mapDiv.style.border = '2px solid blue';

    })();
    window.initMap = function() {
        var myLatLng = {
            lat: 47.361451,
            lng: 8.564295,
        };
        var mapDiv = document.getElementById('myMap')
        var map = new google.maps.Map(mapDiv,{
            zoom: 12,
            center: myLatLng
        });
        googleMap = map;
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Canoo',
            label: 'Canoo',
        });


        var container = document.getElementsByClassName('item-listing')[0];
        console.log(container);
        for (var i = 0; i < container.children.length; i++) {
            var element = container.children[i];
            var urlBlock = container.children[i].getElementsByClassName('item-title')[0];
            //console.log(urlBlock.href);
            var html = getHTML(urlBlock.href, i);
            while (window.google === undefined || window.google === null);
            html.then((data)=>{
                    var form = data.document.getElementById('similarForm');
                    var location = undefined;
                    if (form) {
                        var inputs = form.getElementsByTagName('input');
                        location = {
                            la: inputs[inputs.length - 2].value,
                            lo: inputs[inputs.length - 1].value
                        };
                    } else {
                        var streetView = data.document.getElementsByClassName('google-streetview');
                        if (streetView.length == 1) {
                            var re = /location=([0-9\.]+)\,([0-9\.]+)/;
                            var matches = re.exec(streetView[0].innerHTML);
                            location = {
                                la: matches[1],
                                lo: matches[2]
                            };
                        }
                    }
                    if (location !== undefined && location !== null) {
                        //var g = window.google;
                        //var map = googleMap;
                        var myLatLng = new google.maps.LatLng(location.la,location.lo);

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            title: data.url,
                            label: '' + data.index,
                            map: googleMap
                        });

                        marker.addListener('click', () => { window.open(data.url, '_blank')} );
                    } else {
                        console.log("without map location: " + data.url);
                    }
                }
            );
            //console.log(element);

            var newColumn = document.createElement("div");

            newColumn.setAttribute ('class', 'item-prop-column labeled-prop-column');
            newColumn.innerHTML = i;
            /* var newCell = [];
             for ( var j = 0; j < 3; j++) {
             newCell[j] = document.createElement('div');
             newCell[j].setAttribute('class', 'item-props labeled-props');
             newCell[j].innerHTML = i + '-' + j;
             newColumn.append(newCell[j]);
             }
             */
            element.insertBefore(newColumn,element.children[0]);

        }
    }
})();
