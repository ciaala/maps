console.log('map.js loaded');


function GoogleMapHelper(eventManager) {
    "use strict";
    let isDebugMode = false;
    let gmap;
    let isGoogleMapLoaded = false;
    let isMapDivInserted = false;
    let markerCount = 0;
    const markers = [];
    let directionsService;
    let directionsDisplay;
    const markerCache = new Set();
    eventManager.subscribe(eventManager.events.googleMapScript, setupMap);
    eventManager.subscribe(eventManager.events.mapReady, setupMap);

    function init() {
        let mapDiv = document.getElementById('myMap');
        if (mapDiv === undefined || mapDiv === null) {
            mapDiv = document.createElement('div');
            mapDiv.setAttribute('id', 'myMap');
            mapDiv.style.height = '600px';
            mapDiv.style.width = '1280px';
            mapDiv.style.padding = '8px';
            eventManager.publish(eventManager.events.appendMap, mapDiv);
        }
        let scriptBlock = document.getElementById('scriptBlock');
        if (scriptBlock === null || scriptBlock === undefined) {
            let srcString = 'https://maps.googleapis.com/maps/api/js?key={YOUR_API_KEY}&callback=window.googleMap.initMap';
            //var key = 'AIzaSyBrtWALRhFKan4_v9ELNR_8MIj0Jbv6Bzg';
            let key = 'AIzaSyCvrW2I6KxWZSCWOWAxgSzOZ0nIXmJZDao';
            srcString = srcString.replace('{YOUR_API_KEY}', key);

            scriptBlock = document.createElement('script');
            scriptBlock.setAttribute('id', 'scriptBlock');
            scriptBlock.setAttribute('src', srcString);
            scriptBlock.setAttribute('defer', 'true');
            scriptBlock.setAttribute('async', 'true');

            document.body.append(scriptBlock);
        }

    }

    function calculateDirection(marker) {
        directionsService.route({
                origin: marker.position,
                destination: markers[0].position,
                travelMode: 'TRANSIT',
                transitOptions: {
                    arrivalTime: new Date(2017, 6, 19, 8),
                }
            },
            function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    console.log('Directions request failed due to ' + status);
                }
            }
        )
        ;
    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#444',
            strokeWeight: 1,
            labelOrigin: new google.maps.Point(0, -28),
            scale: 0.8
        };
    }

    function addMarker(eventName, markerData) {
        let latLang = new google.maps.LatLng(
            markerData.latitude,
            markerData.longitude);
        let label;
        if (markerData.label) {
            label = markerData.label;
        }


        let marker = new google.maps.Marker({
            position: latLang,
            title: markerData.title,
            label: {text: label, fontSize: '12px', color: markerData.labelColor, textShadow: '2 2 grey'},
            url: markerData.url,
            visible: true,
            map: gmap,
            icon: pinSymbol(markerData.color)
        });
        marker.setMap(gmap);
        if (markerData.hasOwnProperty('infoPopup')) {

            marker.addListener('click', (contentString => {
                return function () {
                    calculateDirection(marker);
                    let infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    infowindow.open(gmap, marker);
                };
            })(markerData.infoPopup));
        } else {
            marker.addListener('click', () => {
                window.open(markerData.url, '_blank');
            });
        }
        const markerIndex = markers.push(marker);
        let markerId;
        if (markerData.url !== undefined && markerData.url !== null) {
            const markerTokens = markerData.url.split('/');
            markerId = markerTokens[markerTokens.length - 2];
        } else {
            markerId = 'Canoo';
        }
        if (isDebugMode) {
            console.log({
                action: 'register maker',
                markerIndex: markerIndex,
                label: markerData.label,
                markerId: markerId,

                /*markerLatitude: markerData.latitude,
                 markerLongitude: markerData.longitude,*/
            });
            if (markerCache.has(markerId) === true) {
                console.log('already seen marker ' + markerId);
                console.log(markerData);
            } else {
                markerCache.add(markerId);
            }
        }
    }

    function clearMap(eventName, eventData) {
        if (gmap !== null && gmap !== undefined) {
            for (let i = 1; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers.length = 1;
        }
        markerCache.clear();
    }

    this.initMap = function () {
        eventManager.publish(eventManager.events.googleMapScript);
    };

    function setupMap(eventName, eventData) {

        if (eventName === eventManager.events.mapReady) {
            isMapDivInserted = true;
        }
        if (eventName === eventManager.events.googleMapScript) {
            isGoogleMapLoaded = true;
        }
        if (isMapDivInserted === true && isGoogleMapLoaded === true) {

            let mapDiv = document.getElementById('myMap');
            gmap = new google.maps.Map(mapDiv, {
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: {
                    lat: 47.361451,
                    lng: 8.564295,
                },
            });

            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer({preserveViewport: true});
            directionsDisplay.setMap(gmap);
            directionsDisplay.setPanel(document.getElementById('mapsRightPanel'));
            eventManager.publish(eventManager.events.addMarker, {
                    label: 'Canoo',
                    latitude: 47.361451,
                    longitude: 8.564295,
                    color: '#CCF'
                }
            );
            eventManager.subscribe(eventManager.events.addMarker, addMarker);
            eventManager.subscribe(eventManager.events.clearMap, clearMap);
            eventManager.subscribe(eventManager.events.centerMap, centerMap);
            eventManager.publish(eventManager.events.registerButton, {label: 'Clear Map', eventName: 'clearMap'});
        }
    }

    function centerMap(eventName, eventData) {
        gmap.setZoom(12);
        gmap.setCenter(markers[0].position);
    }

    init();
}

function OfferMapper() {
    "use strict";
    let eventManager = new EventManager();

    let websiteAdapter;
//    eventManager.subscribe(eventManager.events.registerButton, {label: 'refresh', handler: refreshData});

    function initializeDataMapper() {
        let websiteAdapter;
        if (window.location.origin.indexOf('immoscout24.ch') !== -1) {
            websiteAdapter = new ImmoScoutAdapter(eventManager);
        } else if (window.location.origin.indexOf('homegate.ch')) {
            websiteAdapter = new HomeGateAdapter(eventManager);
        }
    }

    function init() {
        if (window.googleMap !== undefined && window.googleMap !== null) {
            window.googleMap.reset();
        } else {
            window.googleMap = new GoogleMapHelper(eventManager);
        }
        websiteAdapter = initializeDataMapper();
        eventManager.publish(eventManager.events.registerToolbar, {
            buttons: [
                {
                    label: 'Load Data',
                    eventName: eventManager.events.loadData
                },
                {
                    label: 'Center Map',
                    eventName: eventManager.events.centerMap
                },
            ]
//          eventManager.publish(eventManager.events.registerButton, buttonDefinition);

        });
    }

    this.getEventManager = function () {
        return eventManager;
    };
    init();
}

window.offerMapper = new OfferMapper();
