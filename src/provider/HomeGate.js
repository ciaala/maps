function HomeGateAdapter(eventManager) {
    "use strict";
    const self = this;
    const isDebugMode = false;
    eventManager.subscribe(eventManager.events.registerToolbar, registerToolbar);
    eventManager.subscribe(eventManager.events.appendMap, addMapElement);
    eventManager.subscribe(eventManager.events.loadData, fetchData);

    let toolbar;

    function registerToolbar(eventName, eventData) {
        toolbar = new ToolbarBuilder(eventManager, document.body, eventData);
    }

    function fetchData(eventName, eventData) {
        //let matches = window.location.href.match(/(.*ep=)[1-9][0-9]*$/);

        //if (matches !== null) {
        //    let urlPrefix = matches[1];
        let container = document.getElementsByClassName('paginator-container')[0];
        if (container !== null && container !== undefined) {
            let counter = container.getElementsByClassName('paginator-counter')[0];
            let originalUrl = container.getElementsByClassName('paginator')[0].children[0].children[0].href;
            let matches = originalUrl.match(/^(.*&ep=)[1-9][0-9]*(&.*)?$/);
            if (matches === null) {
                matches = ['', originalUrl + '&ep=', ''];
            } else if (matches.length === 3 && matches[2] === undefined) {
                matches = ['', matches[1], ''];
            }
            if (counter !== null && counter !== undefined) {
                let ranges = counter.getElementsByTagName('span');
                let min = parseInt(ranges[0].innerText);
                let max = parseInt(ranges[1].innerText);
                if (!( min <= max && min === 1 )) {
                    console.log('Error in pagination analysis.');
                    return;
                }
                for (let i = min; i <= max; i++) {
                    let url = matches[1] + i + matches[2];
                    let html = getHTML({
                        url: url,
                        pageIndex: i,
                    });
                    html.then(fetchPage);
                }
            }
        }
        //}

    }

    function rgb2hex(red, green, blue) {
        var rgb = blue + (green * 256) + (red * 65536);
        return '#' + (0x1000000 + rgb).toString(16).slice(1);
    }

    function makeColor(data) {
        return makeColorByPrice(data.price);
    }

    function makeLabel(data) {
        return data.rooms + '/' + data.surface;
    }

    function makeColorByPrice(price) {
        let green, blue, red;
        green = blue = red = 32;
        if (price !== undefined && price !== null) {
            let matches = window.location.href.match(/ag=([1-9][0-9]+)/);
            let min_price = parseInt(matches[1]);
            matches = window.location.href.match(/ah=([1-9][0-9]+)/);
            let max_price = parseInt(matches[1]);
            if (min_price <= max_price) {
                if (price > max_price) {
                    price = max_price;
                }
                let position = (price - min_price) / (max_price - min_price);
                if (position < 0.34) {
                    green = 255 - Math.ceil(3 * 128 * position);
                } else if (position < 0.67) {
                    blue = 255 - Math.ceil(3 * 128 * (position - 0.33));
                } else {
                    red = 255 - Math.ceil(3 * 128 * (position - 0.66));
                }
            }
        }
        return {red: red, green: green, blue: blue};
    }

    function makeGenericLabelColor(backgroundColor) {
        let a = 1 - ( 0.299 * backgroundColor.red + 0.587 * backgroundColor.green + 0.114 * backgroundColor.blue) / 255;
        let labelColor;
        if (a < 0.5) {
            labelColor = 'darkslategrey'; // bright colors - black font
        } else {
            labelColor = 'honeydew'; // dark colors - white font
        }
        return labelColor;
    }
    function makeLabelColor(backgroundColor) {
        if ( backgroundColor.red > 128) {
            return 'seagreen';
        } else if (backgroundColor.green > 128) {
            return 'darkslategrey';
        } else if (backgroundColor.blue > 128) {
            return 'orange';
        } else {
            return 'white';
        }
    }
    function copyProperties(target, source, properties) {
        for (let property of properties) {
            target[property] = source[property];
        }
        return target;
    }

    function handleBothPages(results) {

        const extractedData = extractData(results[0]);
        const allData = results[1];
        extractMapData(allData);
        copyProperties(allData, extractedData, ['infoPopup', 'price', 'surface', 'rooms']);
        let backgroundColor = makeColor(allData);
        allData.color = rgb2hex(backgroundColor.red, backgroundColor.green, backgroundColor.blue);
        allData.labelColor = makeLabelColor(backgroundColor);
        allData.label = makeLabel(allData);
        updateMap(allData);

    }


    function fetchPage(page) {
        let container = page.document.getElementsByClassName('result-item-list')[0];
        if (isDebugMode === true) {
            console.log('fetched page + ' + page.url);
        }
        if (container !== null && container !== undefined) {
            let children = container.getElementsByClassName('box-row-wrapper');


            for (let i = 0; i < children.length; i++) {
                let element = children[i];
                let urlBlock = element.getElementsByClassName('detail-page-link')[0];
                fetchBothPages(urlBlock, element, ((page.pageIndex - 1) * 20) + i);
            }
        }
        return page;
    }

    function fetchBothPages(urlBlock, element, index) {
        let url = urlBlock.href;
        let html1 = getHTML({
            element: element,
            url: url,
            index: index,
            label: '' + index,
        });
        let html2 = getHTML({
            element: element,
            url: url + "/vicinanze",
            index: index,
            label: '' + index
        });
        Promise.all([html1, html2]).then(handleBothPages);
    }

    function extractTextFromDetail(element, selector) {
        let elements = element.querySelectorAll(selector);
        let result = '';
        for (let elm of elements) {
            result += elm.innerText;
        }
        return result;
    }

    function extractDataRows(sourceBlock, destinationBlock) {
        const SELECTOR_NAME = '.text--small';
        const SELECTOR_VALUE = '.float-right.text--shout.text--dark';
        if (sourceBlock !== null && sourceBlock !== undefined) {

            for (let i = 0; i < sourceBlock.childNodes.length; i++) {
                let childNode = sourceBlock.childNodes[i];
                if (childNode.childNodes.length > 0) {
                    const node = childNode.cloneNode(true);
                    let propertyName;
                    let propertyValue;
                    if (node.hasAttribute('item-prop')) {
                        propertyName = node.getAttribute('item-prop');
                        propertyValue = node.innerText;
                    } else {

                        propertyName = extractTextFromDetail(node, SELECTOR_NAME);
                        propertyValue = extractTextFromDetail(node, SELECTOR_VALUE);
                    }
                    node.setAttribute('propertyName', propertyName);
                    node.setAttribute('propertyValue', propertyValue);
                    destinationBlock.appendChild(node);
                }
            }
        }

    }


    function extractPrice(priceDetailBlock) {
        let price = priceDetailBlock.querySelector('[itemprop=price]').innerText;
        return parseInt(price.replace('\'', ''));
    }

    function extractProperty(apartmentDetailBlock, propertyName) {
        let element = apartmentDetailBlock.querySelector('[propertyName=\'' + propertyName + '\']');
        let value = '';
        if (element !== null && element !== undefined) {
            let name = element.getAttribute('propertyName');
            value = element.getAttribute('propertyValue');
        }
        return value;
    }

    function extractData(data) {
        if (data.document !== null && data.document !== undefined) {
            let newBlock = document.createElement('ul');
            newBlock.setAttribute('class', 'list--plain list--flat');
            let priceDetailBlock = data.document.getElementsByClassName('list--plain')[0];
            extractDataRows(priceDetailBlock, newBlock);
            const price = extractPrice(priceDetailBlock);

            let apartmentDetailBlock = data.document.getElementsByClassName('list--plain')[1];
            extractDataRows(apartmentDetailBlock, newBlock);
            let surface = extractProperty(newBlock, 'Superficie abitabile').replace('m2', '');
            let rooms = extractProperty(newBlock, 'Locali');

            let link = document.createElement('a');
            link.addEventListener('click', () => {
                window.open(data.url, '_blank');
            });
            link.innerText = 'link';
            newBlock.appendChild(link);
            return {infoPopup: newBlock, price: price, surface: surface, rooms: rooms};
        }
        return undefined;
    }

    function extractMapData(data) {
        let locationMarker = {};
        let locationElement = data.document.getElementById('map-detail');
        if (locationElement !== null && locationElement !== undefined) {
            locationMarker.longitude = locationElement.getAttribute('data-coords-lon');
            locationMarker.latitude = locationElement.getAttribute('data-coords-lat');
        }
        if (locationMarker === undefined || locationMarker === null) {
            console.log("problem without map location: " + data.url);
            return false;
        } else {
            data.locationMarker = locationMarker;
            return true;
        }
    }

    /*
     function handleSingleOffer(data) {


     let isExtracted = extractData(data);
     if (isExtracted === true) {
     let consumers = [updateMap, updateTable];
     for (let consumer of consumers) {
     consumer(data);
     }
     } else {
     console.log('cannot create a location marker: ' + data)
     }
     }

     function updateTable(data) {

     }
     */
    function updateMap(data) {
        if (data.locationMarker !== undefined && data.locationMarker !== null) {
            let locationMarker = data.locationMarker;
            copyProperties(locationMarker, data, ['url', 'color', 'price', 'infoPopup', 'label', 'index', 'labelColor']);
            eventManager.publish(eventManager.events.addMarker, locationMarker);
        } else {
            console.log('error in update map ' + data);
        }
    }

    function addMapElement(eventName, mapElement) {
        //let titleElement = document.getElementById('listBreadcrumb');
        //let titleElement = document.getElementsByClassName('search-results-header')[0];
        let titleElement = document.getElementsByClassName('regio-star-ad')[0];
        let wrapper = document.createElement('div');
        wrapper.setAttribute('id', 'ffi-wrapper');
        wrapper.style.width = '100%';
        wrapper.style.height = '600px';
        wrapper.style.border = '1px solid blue';
        //wrapper.style.backgroundColor = '#808080';
        //wrapper.style.padding = '4px';
        titleElement.parentNode.insertBefore(wrapper, titleElement);
        titleElement.parentNode.removeChild(titleElement);
        let otherBar = document.getElementsByClassName('search-results-actions')[0];
        otherBar.parentNode.removeChild(otherBar);

        let rightPanel = document.createElement('div');

        rightPanel.setAttribute('id', 'mapsRightPanel');
        mapElement.style.float = 'left';
        mapElement.style.width = '75%';

        wrapper.append(mapElement);
        wrapper.append(rightPanel);
        rightPanel.style.float = 'right';
        rightPanel.style.width = '25%';
        rightPanel.style.paddingLeft = '8px';
        document.styleSheets[0].addRule('#mapsRightPanel .adp-summary', 'font-weight: bold');
        eventManager.publish(eventManager.events.mapReady, mapElement);
    }
}
