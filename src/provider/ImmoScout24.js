
function ImmoScoutAdapter(eventManager) {
    "use strict";
    let toolbar;
    const self = this;
    eventManager.subscribe(eventManager.events.registerToolbar, registerToolbar);
    eventManager.subscribe(eventManager.events.appendMap, addMapElement);
    eventManager.subscribe(eventManager.events.loadData, fetchData);

    function registerToolbar(eventName, eventData) {
        toolbar = new ToolbarBuilder(eventManager, document.body, eventData);
    }

    function updateTable(data) {
        let columns = data.element.getElementsByClassName('ffi-column');
        if (columns.length === 0) {
            let newColumn = document.createElement("div");

            newColumn.setAttribute('class', 'item-prop-column labeled-prop-column');
            newColumn.innerHTML = data.index;
            // var newCell = [];
            //  for ( var j = 0; j < 3; j++) {
            //  newCell[j] = document.createElement('div');
            //  newCell[j].setAttribute('class', 'item-props labeled-props');
            //  newCell[j].innerHTML = i + '-' + j;
            //  newColumn.append(newCell[j]);
            //  }

            data.element.insertBefore(newColumn, data.element.children[0]);
        }
    }

    function updateMap(data) {
        if (data.locationMarker !== undefined && data.locationMarker !== null) {
            let locationMarker = data.locationMarker;
            //locationMarker.label = '' + data.index;
            locationMarker.url = data.url;
            locationMarker.color = data.color;
            locationMarker.title = data.price;
            eventManager.publish(eventManager.events.addMarker, locationMarker);
        } else {
            console.log('error in update map ' + data);
        }
    }

    function extractData(data) {
        let form = data.document.getElementById('similarForm');
        let locationMarker = {};
        if (form) {
            let inputs = form.getElementsByTagName('input');
            locationMarker = {
                latitude: inputs[inputs.length - 2].value,
                longitude: inputs[inputs.length - 1].value,
            };
        } else {
            let streetView = data.document.getElementsByClassName('google-streetview');
            if (streetView.length === 1) {
                let re = /location=([0-9.]+),([0-9.]+)/;
                let matches = re.exec(streetView[0].innerHTML);
                locationMarker = {
                    latitude: matches[1],
                    longitude: matches[2],
                };
            }
        }

        if (locationMarker === undefined || locationMarker === null) {
            console.log("problem without map location: " + data.url);
            return false;
        } else {
            data.locationMarker = locationMarker;
            return true;
        }
    }

    function handleSingleOffer(data) {
        if (extractData(data) === true) {

            let consumers = [updateMap, updateTable];
            for (let consumer of consumers) {
                consumer(data);
            }
        } else {
            console.log('cannot create a location marker: ' + data);
        }
    }

    function fetchData(eventName, eventData) {
        let container = document.getElementsByClassName('item-listing')[0];
        if (container !== null && container !== undefined) {
            let elements = container.getElementsByClassName('item');
            for (let i = 0; i < elements.length; i++) {
                let element = elements[i];
                let urlBlock = element.getElementsByClassName('item-title')[0];
                let html = getHTML({
                    element: element,
                    url: urlBlock.href,
                    index: i
                });
                html.then(handleSingleOffer);
            }
        }
    }


    function addMapElement(eventName, mapElement) {
        let titleElement = document.getElementById('listBreadcrumb');
        // let titleElement = document.getElementsByClassName('site-header')[0];
        titleElement.parentNode.insertBefore(mapElement, titleElement);
        titleElement.parentNode.removeChild(titleElement);
        // titleElement.append(mapElement);
        eventManager.publish(eventManager.events.mapReady, mapElement);
    }
}
