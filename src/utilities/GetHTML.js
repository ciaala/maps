
function getHTML(data) {
    "use strict";
    const isDebugMode = false;
    let request = new XMLHttpRequest();
    request.responseType = 'document';

    return new Promise(function (resolve, fail) {
        request.onload = function () {
            if (isDebugMode === true) {
                console.log('response ' + data.index + ' ' + data.url);
            }
            data.document = request.responseXML;
            return resolve(data);
        };
        request.onerror = function () {
            return fail("error");
        };
        request.open("GET", data.url, true);
        request.send();
    });
}
