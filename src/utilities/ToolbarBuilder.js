
function ToolbarBuilder(eventManager, parentNode, toolbarOptions) {
    "use strict";
    const self = this;

    let toolbar = document.getElementById('ffi-toolbar');

    function registerButton(eventName, buttonOptions) {
        let buttonElement = document.createElement('button');
        buttonElement.setAttribute('value', buttonOptions.label);
        buttonElement.style.backgroundColor = '#ff9800';
        buttonElement.style.margin = '0px 8px 0px 8px';
        buttonElement.style.padding = '8px';
        buttonElement.style.border = 'none';
        buttonElement.style.fontSize = '16px';
        buttonElement.innerText = buttonOptions.label;
        buttonElement.addEventListener('click', () => {
            eventManager.publish(buttonOptions.eventName);
        });
        toolbar.append(buttonElement);

    }

    function init() {
        if (toolbar === null || toolbar === undefined) {
            toolbar = document.createElement('div');
            toolbar.setAttribute('id', 'ffi-toolbar');
            toolbar.style.backgroundColor = '#ef6c00';
            toolbar.style.height = '36px';
            toolbar.style.width = '100%';
            toolbar.style.position = 'fixed';
            toolbar.style.zIndex = 1000;
            parentNode.insertBefore(toolbar, parentNode.children[0]);
            eventManager.subscribe(eventManager.events.registerButton, registerButton);
            for (let buttonOptions of toolbarOptions.buttons) {
                eventManager.publish(eventManager.events.registerButton, buttonOptions);
            }
        }

    }

    init();
}
